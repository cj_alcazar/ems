package employees

import java.time.LocalDate
import kotlin.random.Random

abstract class Employee(
    override var firstName: String,
    override var lastName: String,
    var department: String,
    var hiringDate: LocalDate,
    var location: LocationDetails) : EmployeeInterface{
    val id: String
    var rand = String.format("%8d", Random.nextInt(99999999))
    // Create a  function that shows the data of this employee
    fun showEmployeeData() = println("Employee Name: $lastName, $firstName from $department Department ")
    /** Generate an id, and save as an attribute of this class
    the id format is : First 2 letters of the firstName + - + first
    2 letters of the lastname + - + 8 random digits
    **/
//    override
//    fun doSomething(): String = "Sample String"

    init {
        this.id = "${firstName.substring(0,2).uppercase()}-${lastName.substring(0,2).uppercase()}-${this.rand}"
    }
}