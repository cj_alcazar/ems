package employees
/**
 * A class containing all the details about the office
 * location of an employee
 * City, Province, Region, Exact Address
 **/

// Data Class -> automatic string conversion (toString())
// equal () checks an actual data
// we can use copy()
data class LocationDetails (
    val city: String,
    val province: String,
    val region: String,
    val exactAddress: String){
}

// Stringified value of the instantiated Object (JSON Like)
// LocationDetails(city=Makati, province=Metro Manilla, region=NCR, exactAddress=6578 HJK Building, Legaspi Village)
// Map version of the object above
// {city=Makati, province=Metro Manilla, region=NCR, exactAddress=6578 HJK Building, Legaspi Village}