package employees

import java.time.LocalDate

fun main(){
//    val employee2 = AdminEmployee("Brandon","Cruz","HR", LocalDate.now())
//
//    println(employee2.showEmployeeData())
//    println(employee2.isRankAndFile)
//    println(employee2.isAdmin)
//    println(employee2.id)
//    //with, apply, let, run
//
//    with(employee2){
//        println(firstName)
//        println(lastName)
//        println(id)
//        println(department)
//        println(isRankAndFile)
//        println(isAdmin)
//    }
//    employee2.apply {
//        this.firstName = firstName.uppercase()
//    }
//    println(employee2.firstName)
    val address1 = LocationDetails("Makati",
        "Metro Manila",
        "NCR",
        "1124 XYZ Building, Legaspi Village")
//    val address2 = LocationDetails("Makati",
//        "Metro Manila",
//        "NCR",
//        "1124 XYZ Building, Legaspi Village")
    val employee1 = RankAndFileEmployee("Amy","Carter","IT", LocalDate.now(), address1)
//    val employee2 = RankAndFileEmployee("Amy","Carter","IT", LocalDate.now(), address2)

    val address2 = address1.copy(
        exactAddress = "6578 HJK Building, Legaspi Village"
    )
    println(employee1.location.exactAddress)
    println(address2)
}
