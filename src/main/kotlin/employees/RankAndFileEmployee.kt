package employees

import java.time.LocalDate

class RankAndFileEmployee (
        firstName: String,
        lastName: String,
        department: String,
        hiringDate: LocalDate,
        location: LocationDetails
):
        Employee(firstName,
                lastName,
                department,
                hiringDate,
                location){

        val isRankAndFile : Boolean = true
        val isAdmin : Boolean = false
        override
        fun doSomething(): String = isRankAndFile.toString()
}