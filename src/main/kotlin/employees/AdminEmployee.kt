package employees

import java.time.LocalDate
import javax.xml.stream.Location

class AdminEmployee(
    firstName: String,
    lastName: String,
    department: String,
    hiringDate: LocalDate,
    location: LocationDetails

):
    Employee(firstName,
        lastName,
        department,
        hiringDate,
        location
) {
    val isRankAndFile : Boolean = false
    val isAdmin : Boolean = true
    override
    fun doSomething(): String = isAdmin.toString()
}